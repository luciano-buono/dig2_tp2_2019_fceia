#include "fsl_debug_console.h"
#include "clock_config.h"
#include "fsl_smc.h"




void APP_SetClockVlpr(void);

void APP_SetClockRunFromVlpr(void);
/*! @brief Show current power mode. */
void APP_ShowPowerMode(smc_power_state_t currentPowerState);

