/* Copyright 2018, DSI FCEIA UNR - Sistemas Digitales 2
 *    DSI: http://www.dsi.fceia.unr.edu.ar/
 * Copyright 2018, Gustavo Muro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

// Standard C Included Files
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>


// Project Included Files
#include "SD2_board.h"
#include "mma8451.h"
#include "Clock.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/
typedef enum
{
	STBY = 0,
	CONV,
	SHOW,

}estado_enum;

uint8_t temp8;

int Parpadeo_Rojo=0;
uint16_t Lecturamaxima,Conversiones_En_reposo, Lectura_actual;

estado_enum estado=STBY;
int cont10s=0,reposo=0;
double TEST;
double aux1;
float aux2;

/*==================[internal functions declaration]=========================*/
void PIT_Init(void);
void Mostrar_LCD(uint16_t dato);
void ClockInitFromVLPR();
void ClockSetRunMode();
void SysTick_Init(void);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	int16_t aux=0;

	board_init(); // Se inicializan funciones de la placa
	SysTick_Init();


	key_init();// Inicializa keyboard
	mma8451_init();  //Start as FF
	ClockInitFromVLPR();//Empezamos en VLPR



    while(1)
    {
		switch(estado){
		case STBY:
			Parpadeo_Rojo=0;
			mma8451_setLectMaxTo0();
			board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_OFF);
			board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_OFF);

			aux= mma8451_get_IntFlag_FF(); //Interrupt Flag
			if( aux ){  //comprobar que en modo FF
				ClockSetRunMode();

				Parpadeo_Rojo=1;
				mma8451_2DRDY();  //ahora accel en modo DRDY

				estado=CONV;
				}
			break;




		case CONV:


			if(mma8451_get_Conv_En_Reposo ()){  //Return true if the last 20 conversion were idle
				mma8451_2FF();//modo freefall para dejar de hacer conversiones
				estado=SHOW;
				PRINTF( "Maxima aceleracion: %d\n", (int)sqrt(  mma8451_getLectMaxCuad()  )  ) ;

				}
			break;

		case SHOW:
			board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_ON);
			if(cont10s >= 10000  ||   key_getPressEv(BOARD_SW_ID_1)){//10s
				cont10s=0;
				estado=STBY;
				mma8451_2FF();
				ClockInitFromVLPR();//Empezamos en VLPR


			}
			break;

		default:
			estado=STBY;

		}

    }

}





void Mostrar_LCD(uint16_t dato){
	uint8_t BufLCD[6];
	sprintf((char *)BufLCD,"%4d",dato);
	vfnLCD_Write_Msg(BufLCD);
}


void SysTick_Init(void){
	SysTick_Config(SystemCoreClock / 1000U);
}

void SysTick_Handler(void){
	static int cont_led_toggle=0;

	key_periodicTask1ms();
	if(Parpadeo_Rojo){  //SI esta ON LED Parpadea
		cont_led_toggle++;
		if(cont_led_toggle >= 1000){
			board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_TOGGLE);
			cont_led_toggle=0;
		}
	}

	if(estado == SHOW){

		cont10s++;
	}
}


void ClockInitFromVLPR(){
	uint32_t freq = 0;
	smc_power_state_t currentPowerState;
	/* Init FSL debug console. */
	BOARD_InitDebugConsole();

    /* Se habilita la posibilidad de operar con todos los modos de bajo consumo */
    SMC_SetPowerModeProtection(SMC, kSMC_AllowPowerModeAll);


	/* Init board hardware. */
    BOARD_InitBootClocks();

    APP_SetClockVlpr();
    currentPowerState = SMC_GetPowerModeState(SMC);
	APP_ShowPowerMode(currentPowerState);

	freq = CLOCK_GetFreq(kCLOCK_CoreSysClk);
	PRINTF("    Core Clock = %dHz \r\r", freq);

}

void ClockSetRunMode(){
	uint32_t freq = 0;
	smc_power_state_t currentPowerState;
	APP_SetClockRunFromVlpr();

	currentPowerState = SMC_GetPowerModeState(SMC);
	APP_ShowPowerMode(currentPowerState);

	freq = CLOCK_GetFreq(kCLOCK_CoreSysClk);
	PRINTF("    Core Clock = %dHz \r\r", freq);

}

/*==================[end of file]============================================*/


